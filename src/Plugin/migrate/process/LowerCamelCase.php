<?php

namespace Drupal\migrate_process_lowercamelcase\Plugin\migrate\process;

use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * This plugin transforms text to lower camel case format.
 *
 * Example, transform the string "How Now Brown Cow" to "howNowBrownCow".
 *
 * @MigrateProcessPlugin(
 *   id = "lower_camel_case"
 * )
 *
 * To do custom value transformations use the following:
 *
 * @code
 * field_text:
 *   plugin: lower_camel_case
 *   source: text
 * @endcode
 *
 */
class LowerCamelCase extends ProcessPluginBase {

  /**
   * Regex pattern for characters to explode on.
   */
  const PATTERN_MATCH = "/[^a-zA-Z0-9]+/";

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $values = $this->createArrayFromString($value);
    $values = $this->upperCaseWords($values);
    $values = $this->firstWordLowerCase($values);

    return $this->constructStringFromArray($values);
  }

  /**
   * Create an array from a string.
   *
   * First step to transforming the string.
   *
   * @param string $value
   *   The string to transform.
   *
   * @return array
   *   The string split up into an array.
   *
   * @throws \Drupal\migrate\MigrateException
   */
  private function createArrayFromString($value) {
    $values = preg_split(self::PATTERN_MATCH, $value);

    if (!is_array($values)) {
      throw new MigrateException('Error creating an array from string.');
    }

    return $values;
  }

  /**
   * Transform first letter of words upper case.
   *
   * Second step to transforming the string.
   *
   * @param array $values
   *   The array of strings to transform.
   *
   * @return array
   *   Array of upper case words.
   */
  private function upperCaseWords(array $values) {
    return array_map(function ($value) {
      return ucfirst(strtolower($value));
    }, $values);
  }

  /**
   * Transform first word in array to all lower case.
   *
   * Third step to transforming the string.
   *
   * @param array $values
   *   The array to transform.
   *
   * @return array
   *   Array with first word all lower case.
   */
  private function firstWordLowerCase(array $values) {
    $values[0] = strtolower($values[0]);

    return $values;
  }

  /**
   * Create string from an array.
   *
   * Fourth step to transforming the string.
   *
   * @param array $values
   *   The array of strings to transform.
   *
   * @return string
   *   The newly transformed string as lowerCamelCase.
   */
  private function constructStringFromArray(array $values) {
    return implode($values);
  }

}
