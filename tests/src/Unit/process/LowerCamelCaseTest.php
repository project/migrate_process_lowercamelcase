<?php

namespace Drupal\Tests\migrate_process_lowercamelcase\Unit\process;

use Drupal\migrate_process_lowercamelcase\Plugin\migrate\process\LowerCamelCase;
use Drupal\Tests\migrate\Unit\process\MigrateProcessTestCase;

/**
 * Tests the lowerCamelCase process plugin.
 *
 * @group migrate
 * @coversDefaultClass \Drupal\migrate_process_lowercamelcase\Plugin\migrate\process\LowerCamelCase
 */
class LowerCamelCaseTest extends MigrateProcessTestCase {

  /**
   * Test for a simple lowerCamelCase string.
   */
  public function testLowerCamelCase() {
    $value = 'How now brown cow';
    $plugin = new LowerCamelCase([], 'lower_camel_case', []);
    $actual = $plugin->transform($value, $this->migrateExecutable, $this->row, 'destinationproperty');
    $this->assertSame('howNowBrownCow', $actual);
  }

}
